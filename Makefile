SYS := $(shell uname)
NAME = wolf3d

# directories
SRCDIR = ./src
INCDIR = ./include
OBJDIR = ./obj

SRC	=	main.c \
		actions/collisions.c \
		actions/events.c \
		finalization/cleanup.c \
		finalization/termination.c \
		initialization/camera.c \
		initialization/config.c \
		initialization/context.c \
		initialization/map_validation.c \
		initialization/map_parsing.c \
		initialization/textures.c \
		multithreaded/minimap/map_render.c \
		multithreaded/minimap/fov_line_render.c \
		multithreaded/render/intersection_closest.c \
		multithreaded/render/ray_casting.c \
		multithreaded/render/intersections.c \
		multithreaded/render/pixel_mapping.c \
		multithreaded/render/render_ray_directions.c \
		multithreaded/render.c
		

OBJ = $(addprefix $(OBJDIR)/,$(SRC:.c=.o))

# compiler
CC		=	gcc
CFLAGS	=	-Wall -Wextra -Werror
CFLAGS	+=	-Ofast 
CFLAGS	+=	-g

#LIBFT
LIBFT_DIR 	= ./lib/libft 
LIBFT		= ./lib/libft/libft.a
LIBFT_INC	= -I ./lib/libft/include
LIBFT_LNK	= -L ./lib/libft -lft

#SDL
SDL_INC	= -I ./include/SDL2
ifeq ($(SYS), Linux)
	SDL		= Makefile
	SDL_LNK = -lSDL2
	LNK += -lpthread
else
	SDL		= ~/lib/libSDL2-2.0.0.dylib
	SDL_LNK	= -L ~/lib -lSDL2-2.0.0
endif

LNK	+=	$(LIBFT_LNK)
LNK	+=	$(SDL_LNK)

INC =	-I ./include
INC	+=	$(LIBFT_INC)
INC	+=	$(SDL_INC)


all: obj $(LIBFT) $(SDL) $(NAME)

obj:
	mkdir -p $(OBJDIR)
	mkdir -p $(OBJDIR)/actions
	mkdir -p $(OBJDIR)/finalization
	mkdir -p $(OBJDIR)/initialization
	mkdir -p $(OBJDIR)/multithreaded/minimap
	mkdir -p $(OBJDIR)/multithreaded/render
	

$(OBJDIR)/%.o:$(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

$(LIBFT):
	make -C $(LIBFT_DIR)

$(SDL):
	./install_sdl2_mac.sh

$(NAME): $(OBJ)
	$(CC) $(OBJ) -lm -o $@ $(LNK)

clean:
	rm -rf $(OBJDIR)
	make -C $(LIBFT_DIR) clean

fclean: clean
	rm -rf $(NAME)
	make -C $(LIBFT_DIR) fclean

cleanobj:
	rm -rf $(OBJDIR)

resdl:
	./install_sdl2_mac.sh

repair: cleanobj all

re: fclean all
