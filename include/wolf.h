/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 17:22:05 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:25:45 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# define _USE_MATH_DEFINES
# include <math.h>
# include <stdint.h>
# include <stdbool.h>
# include <pthread.h>
# include <fcntl.h>
# include <time.h>
# include "SDL2/SDL.h"
# include "libft.h"
# include "wolf_enums.h"

# define NUM_THREADS 8
# define SCREEN_WIDTH 1600
# define SCREEN_HEIGHT 900
# define COLOR_SKY 0x8B6F3
# define COLOR_GROUND 0x7D3A11
# define COLOR_WALL 0x6B9494
# define MM_SIZE 200
# define MM_COLOR_WALL 0xA63E00
# define MM_COLOR_DOOR 0x48036F
# define MM_COLOR_PLAYER 0x00080B74
# define MM_COLOR_BACKGROUND 0x00750062
# define MM_COLOR_FOV 0xFF0000
# define CLOSED_DOOR 2
# define OPEN_DOOR 3

typedef struct	s_process_door_data
{
	uint32_t	new_x;
	uint32_t	new_y;
	uint32_t	index;
	uint32_t	tile_value;
	bool		player_not_in_doorway;
	bool		tile_is_door;
}				t_process_door_data;

typedef struct	s_texture
{
	const uint32_t	*pixels;
	uint32_t		width;
	uint32_t		height;
}				t_texture;

typedef struct	s_ver_line
{
	float		distance;
	uint32_t	x;
	uint32_t	top_y;
	uint32_t	height;
	t_texture	*text;
}				t_ver_line;

typedef struct	s_rectangle
{
	uint16_t	left_x;
	uint16_t	top_y;
	uint16_t	width;
	uint16_t	height;
	uint32_t	color_argb;
}				t_rectangle;

typedef struct	s_camera
{
	float		dir_angle;
	float		dir_cos;
	float		dir_sin;
	float		pos_x;
	float		pos_y;
}				t_camera;

typedef struct	s_map
{
	uint32_t	*array;
	uint32_t	rows;
	uint32_t	cols;
}				t_map;

typedef struct	s_ray
{
	float		dir_angle;
	float		unreduced_angle;
	float		center_angle;
	int32_t		x_dir;
	int32_t		y_dir;
	float		init_x;
	float		init_y;
}				t_ray;

typedef struct	s_intersection
{
	uint32_t	current_tile_x;
	uint32_t	current_tile_y;
	float		current_pos_x;
	float		current_pos_y;
	float		step_x;
	float		step_y;
	float		abs_dist_x;
}				t_intersection;

typedef struct	s_config
{
	char		*map_path;
	char		*dir_angle;
	char		*pos_x;
	char		*pos_y;
}				t_config;

typedef struct	s_context
{
	bool			running;
	SDL_Window		*win;
	uint32_t		*image;
	t_camera		*cam;
	t_map			*map;
	t_texture		**textures;
	t_rectangle		floor;
	t_rectangle		ceiling;
}				t_context;

typedef struct	s_thread
{
	uint32_t		num;
	pthread_t		pthread;
	t_context		*context;
	void			*error;
}				t_thread;

typedef struct	s_first_intersection_data
{
	uint32_t	index;
	uint32_t	map_mem_size;
	uint32_t	current_tile_x;
	uint32_t	current_tile_y;
	uint32_t	abs_ver_step_x;
	uint32_t	abs_hor_step_x;
}				t_first_intersection_data;

typedef struct	s_texture_mapping_data
{
	float		text_y;
	uint32_t	text_max_y;
	float		text_step_y;
	uint32_t	text_x;
	uint32_t	image_y;
	uint32_t	image_max_y;
	uint32_t	image_x;
}				t_texture_mapping_data;

typedef struct	s_rectangle_render_data
{
	uint32_t	x;
	uint32_t	y;
	uint32_t	max_x;
	uint32_t	max_y;
	uint32_t	row_offset;
}				t_rectangle_render_data;

typedef struct	s_multithreading_data
{
	t_context	rendering_context;
	t_camera	rendering_camera;
	t_map		rendering_map;
	t_thread	threads[NUM_THREADS];
}				t_multithreading_data;

/*
** context related functions
*/
t_context		*init_context(void);
void			cleanup_context(t_context *context);

/*
** event related functions
*/
void			*render_multithreaded(t_context *context);
int32_t			process_keys_state(t_map *map, t_camera *cam,
					const uint8_t *keys_state);
void			terminate_with_error(const char *error);

/*
** render related functions
*/
void			update_window_surface(t_context *context);
void			render_minimap(t_context *context);
void			render_rectangle_surface(
						t_context *context, t_rectangle *rect);
void			render_textured_line(uint32_t *image, t_ver_line *line,
						float intersection_coordinate);
void			*cast_rays(t_thread *thread);
void			calc_ver_intersection(t_intersection *intersect,
									t_ray *ray);
void			calc_hor_intersection(t_intersection *intersect,
									t_ray *ray);
void			set_ray_directions(t_ray *ray);

/*
** minimap related functions
*/
void			render_line(t_context *context, float angle);

/*
** environment related functions
*/
t_map			*read_map_file(const char *map_path);
int32_t			collides_with_walls(t_camera *cam, t_map *map, t_movement type);
void			validate_map_and_camera(const t_map *map, const t_camera *cam);
void			check_config_required_fields(t_config *cfg);

/*
** intersections calculations
*/
t_intersection	*closest_wall_intersection(
	t_intersection *ver, t_intersection *hor, t_map *map, t_ray *ray);
void			first_horizontal_tile_intersection(
	t_intersection *i, t_ray *ray);
void			first_vertical_tile_intersection(
	t_intersection *i, t_ray *ray);

/*
** texture related functions
*/
t_texture		**cache_textures_from_files();
void			render_texture(t_texture *texture);

/*
** initialization functions
*/
t_camera		*init_camera(const t_config *cfg);
t_config		*read_config_file(void);
t_map			*read_map_file(const char *map_path);
t_texture		**read_texture_files();

#endif
