/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf_enums.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 18:15:10 by eloren-l          #+#    #+#             */
/*   Updated: 2019/10/27 15:05:50 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_ENUMS_H
# define WOLF_ENUMS_H

typedef	enum	e_implement
{
	none_implement,
	surface,
	renderer_pixel,
	renderer_rect
}				t_implement;

typedef enum	e_renderer
{
	none_renderer,
	software,
	hardware
}				t_renderer;

typedef enum	e_movement
{
	FORWARDS,
	BACKWARDS,
	LEFT,
	RIGHT
}				t_movement;

typedef enum	e_textures_names
{
	BRICK,
	COBB,
	DOOR,
	MOSS,
	PLANK
}				t_textures_names;

#endif
