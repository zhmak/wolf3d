/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 16:46:29 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:43:10 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdint.h>
# include <stdbool.h>

# define BUFF_SIZE 128

typedef struct		s_descr
{
	int				fd;
	char			*rem;
	struct s_descr	*next;
}					t_descr;

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_mem32i
{
	int32_t			*content;
	uint64_t		capacity;
	bool			wrapped;
}					t_mem32i;

int					get_next_line(const int fd, char **line);

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

size_t				ft_strlen(const char *s);
char				*ft_strdup(const char *s1);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strnstr(const char *haystack,
								const char *needle, size_t len);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);

int					ft_atoi(const char *str);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);

void				*ft_memalloc(size_t size);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
char				*ft_itoa(int n);

void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);

t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

void				free_words(char **words);
size_t				count_words(char **words);
int					ft_atoi_16(const char *str);
float				ft_atof(char *str);

/*
** checks if @p is NULL or *@p is NULL
** if not calls free(*@p) and sets *@p = NULL
*/
void				memdel(void **p);

/*
** if @result == 0 prints @err in stderr and exits program with failure
*/
void				assert_true(bool result, const char *err);

/*
** if @ptr is NULL prints @err in stderr and exits program with failure
** otherwise returns @ptr
*/
void				*assert_not_null(const void *ptr, const char *err);

/*
** allocates @num_of_bytes with $p = malloc(@num_of_bytes)
** calls assert_not_null($p, @err) and to check for null
*/
void				*assert_malloc(size_t num_of_bytes, const char *err);

/*
** calls $p = assert_malloc(@num_of_bytes, @err)
** calls ft_bzero($p) and returns $p
*/
void				*assert_malloc_zero(size_t num_of_bytes, const char *err);

/*
** attempts to open @path with $fd =open(@path, O_RDNLY)
** if $fd == -1 prints @err in stderr and exits program with failure
** otherwise returns $fd
*/
int					try_open_read(const char *path, const char *err);

/*
** outputs @exc to stderr, template "[EXCEPTION]\n" or default exc
** outputs @err to stderr, template "ERROR ERROR\n" or default err
** exits program with EXIT_FAILURE
*/
void				throw_exc(char *exc, char *err);
/*
** returns -1 instead of exiting
*/
int					throw_soft_exc(char *exc, char *err);

/*
** returns fractional part of a floating pointer number @n
*/
float				ft_modf(float n);

/*
** allocates memory for the struct and @size * sizeof(type) to store values
** if memory allocation fails at any step returns NULL
** returns pointer to mem32i struct
*/
t_mem32i			*alloc_mem32i(uint64_t size);

/*
** allocates memory for the struct, if memory allocation fails returns NULL
** uses memory from @memory of size @size to store values
** returns pointer to mem32i struct
*/
t_mem32i			*wrap_mem32i(int32_t *memory, uint64_t size);

/*
** frees struct and its memory and returns true
** if incorrect struct or NULL pointer is provided returns false
** !WILL NOT FREE BACKING MEMORY IF STRUCT WAS CREATED WITH wrap_mem32i(...)!
*/
bool				free_mem32i(t_mem32i **mem);

/*
** inserts @value in backing memory at @index and returns 0
** if @mem struct or backing memory is NULL returns -1
** if @index is out of bounds of backing memory returns -2
*/
int32_t				insert_mem32i(t_mem32i *mem, int32_t value, uint64_t index);

/*
** gets value from backing memory at @index, sets @error to 0 and returns value
** if @mem struct or backing memory is NULL sets @error to -1
** if @index is out of bounds of backing memory sets @error -2
** in case of errors returns INT32_MIN
*/
int32_t				get_mem32i(t_mem32i *mem, uint64_t index, int32_t *error);

/*
** returns capacity of backing memory
** if @mem is NULL returns 0
*/
uint64_t			size_mem32i(t_mem32i *mem);

/*
** returns true if created with wrap_mem32i(...) otherwise returns false
** if @meme is NULL returns false
*/
bool				is_wrapped_mem32i(t_mem32i *mem);

#endif
