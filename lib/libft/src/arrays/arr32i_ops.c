/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arr32i_ops.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 19:01:49 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:39:36 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int32_t		insert_mem32i(t_mem32i *mem, int32_t value, uint64_t index)
{
	if (mem == NULL || mem->content == NULL)
	{
		return (-1);
	}
	if (index >= mem->capacity)
	{
		return (-2);
	}
	mem->content[index] = value;
	return (0);
}

int32_t		get_mem32i(t_mem32i *mem, uint64_t index, int32_t *error)
{
	if (mem == NULL || mem->content == NULL)
	{
		if (error != NULL)
		{
			*error = -1;
		}
		return (INT32_MIN);
	}
	if (index >= mem->capacity)
	{
		if (error != NULL)
		{
			*error = -2;
		}
		return (INT32_MIN);
	}
	if (error != NULL)
	{
		*error = 0;
	}
	return (mem->content[index]);
}
