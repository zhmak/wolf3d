/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arr32i_state.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 19:01:49 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:35:58 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_mem32i	*alloc_mem32i(uint64_t size)
{
	t_mem32i	*mem;

	if ((mem = malloc(sizeof(t_mem32i))) == NULL)
	{
		return (NULL);
	}
	if ((mem->content = malloc(sizeof(int32_t) * size)) == NULL)
	{
		free(mem);
		return (NULL);
	}
	mem->capacity = size;
	mem->wrapped = false;
	return (mem);
}

t_mem32i	*wrap_mem32i(int32_t *memory, uint64_t size)
{
	t_mem32i	*mem;

	if (memory == NULL)
	{
		return (NULL);
	}
	if ((mem = malloc(sizeof(t_mem32i))) == NULL)
	{
		return (NULL);
	}
	mem->content = memory;
	mem->capacity = size;
	mem->wrapped = true;
	return (mem);
}

bool		free_mem32i(t_mem32i **mem)
{
	if (mem == NULL || *mem == NULL || (*mem)->content == NULL)
	{
		return (false);
	}
	if ((*mem)->wrapped == false)
	{
		free((*mem)->content);
	}
	free(*mem);
	*mem = NULL;
	return (true);
}

uint64_t	size_mem32i(t_mem32i *mem)
{
	if (mem == NULL)
	{
		return (0);
	}
	return (mem->capacity);
}

bool		is_wrapped_mem32i(t_mem32i *mem)
{
	if (mem == NULL)
	{
		return (false);
	}
	return (mem->wrapped);
}
