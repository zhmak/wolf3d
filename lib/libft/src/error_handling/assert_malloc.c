/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   assert_malloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 17:34:21 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:31:39 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*assert_malloc(size_t num_of_bytes, const char *err)
{
	void *p;

	p = NULL;
	if (!(p = malloc(num_of_bytes)))
	{
		if (err)
			ft_putstr_fd(err, 2);
		else
			ft_putstr_fd("Error", 2);
		exit(0);
	}
	return (assert_not_null(p, err));
}
