/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   assert_malloc_zero.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 19:10:56 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:31:29 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*assert_malloc_zero(size_t num_of_bytes, const char *err)
{
	void *p;

	p = assert_malloc(num_of_bytes, err);
	ft_bzero(p, num_of_bytes);
	return (p);
}
