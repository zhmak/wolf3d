/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   assert_not_null.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 17:13:47 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:31:15 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*assert_not_null(const void *ptr, const char *err)
{
	if (ptr != NULL)
	{
		return ((void *)ptr);
	}
	ft_putstr_fd(
		"[NULL POINTER EXCEPTION] - "
		"RECEIVED NULL POINTER WHEN NON-NULL VALUE WAS EXPECTED", 2);
	if (err == NULL)
	{
		ft_putstr_fd("\n[NO ERROR MESSAGE WAS PROVIDED]", 2);
	}
	else
	{
		ft_putstr_fd("\n[ERROR MESSAGE PROVIDED] - ", 2);
		ft_putstr_fd(err, 2);
	}
	exit(EXIT_FAILURE);
}
