/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   assert_true.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 17:13:47 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:30:43 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	assert_true(bool result, const char *err)
{
	if (result != 0)
	{
		return ;
	}
	ft_putstr_fd(
		"[ASSERTION EXCEPTION] - "
		"ASSERTION FAILED\n", 2);
	if (err == NULL)
	{
		ft_putstr_fd("[NO ERROR MESSAGE WAS PROVIDED]\n", 2);
	}
	else
	{
		ft_putstr_fd("[ERROR MESSAGE PROVIDED] - ", 2);
		ft_putstr_fd(err, 2);
	}
	exit(EXIT_FAILURE);
}
