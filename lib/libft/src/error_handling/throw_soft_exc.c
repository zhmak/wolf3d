/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   throw_soft_exc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 17:13:47 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:18:15 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		throw_soft_exc(char *exc, char *err)
{
	if (exc)
	{
		ft_putstr_fd(exc, 2);
		ft_putstr_fd(" ", 2);
	}
	else
	{
		ft_putstr_fd("[UNIDENTIFIED EXCEPTION] ", 2);
	}
	if (err == NULL)
	{
		ft_putstr_fd("NO ERROR MESSAGE WAS PROVIDED\n", 2);
	}
	else
	{
		ft_putstr_fd(err, 2);
		ft_putstr_fd("\n", 2);
	}
	return (-1);
}
