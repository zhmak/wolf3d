/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   try_open_read.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 17:13:47 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:29:27 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fcntl.h"

int	try_open_read(const char *path, const char *err)
{
	int	fd;

	if (path != NULL)
	{
		fd = open(path, O_RDONLY);
		if (fd != -1)
		{
			return (fd);
		}
		ft_putstr_fd(
		"[I/O EXCEPTION] - "
		"FAILED TO OPEN FILE WITH PATH: ", 2);
		ft_putstr_fd(path, 2);
		if (err == NULL)
		{
			ft_putstr_fd("\n[NO ERROR MESSAGE WAS PROVIDED]\n", 2);
		}
		else
		{
			ft_putstr_fd("\n[ERROR MESSAGE PROVIDED] - ", 2);
			ft_putstr_fd(err, 2);
		}
	}
	exit(EXIT_FAILURE);
}
