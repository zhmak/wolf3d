/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_words.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 19:47:59 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:12:37 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	count_words(char **words)
{
	char **start;

	if (words == NULL)
		return (0);
	start = words;
	while (*words)
		words++;
	return (words - start);
}
