/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collisions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 17:55:04 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:10:25 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define SQRT2 1.414213562373095048801688724209698078569671875376948073176679737
#define EXC_BOUNDS	"[ARRAY OUT OF BOUNDS]\n"
#define ERR_MAP	"MAP MEM INDEX OUT OF BOUND ON CLOSEST INTERSECTION SEARCH\n"

static int32_t	check_map_index_for_wall(t_map *map, uint32_t x, uint32_t y)
{
	uint32_t index;

	index = x + y * map->cols;
	if (index >= map->cols * map->rows)
	{
		throw_soft_exc(EXC_BOUNDS, ERR_MAP);
		return (-1);
	}
	if (map->array[index] == 1 || map->array[index] == 2)
	{
		return (1);
	}
	return (0);
}

static int32_t	check_direction(t_camera *cam, t_map *map, float dir_angle)
{
	int32_t		result;
	uint32_t	new_x;
	uint32_t	new_y;

	new_x = cam->pos_x + cosf(dir_angle) / 4;
	new_y = cam->pos_y + sinf(dir_angle) / 4;
	if ((result = check_map_index_for_wall(map, new_x, new_y)))
		return (result);
	new_x = cam->pos_x + cosf(dir_angle + M_PI / 4) / 4 * SQRT2;
	new_y = cam->pos_y + sinf(dir_angle + M_PI / 4) / 4 * SQRT2;
	if ((result = check_map_index_for_wall(map, new_x, new_y)))
		return (result);
	new_x = cam->pos_x + cosf(dir_angle - M_PI / 4) / 4 * SQRT2;
	new_y = cam->pos_y + sinf(dir_angle - M_PI / 4) / 4 * SQRT2;
	if ((result = check_map_index_for_wall(map, new_x, new_y)))
		return (result);
	return (0);
}

int32_t			collides_with_walls(t_camera *cam, t_map *map, t_movement move)
{
	if (move == FORWARDS)
		return (check_direction(cam, map, cam->dir_angle));
	else if (move == BACKWARDS)
		return (check_direction(cam, map, cam->dir_angle + M_PI));
	else if (move == RIGHT)
		return (check_direction(cam, map, cam->dir_angle + M_PI / 2));
	else if (move == LEFT)
		return (check_direction(cam, map, cam->dir_angle - M_PI / 2));
	return (false);
}
