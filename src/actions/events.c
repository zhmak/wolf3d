/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 22:12:27 by hfrankly          #+#    #+#             */
/*   Updated: 2019/12/14 17:08:56 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define EXC_BOUNDS	"[ARRAY OUT OF BOUNDS]"
#define ERR_MAP	"MAP MEM INDEX OUT OF BOUNDS WHILE CHEKING EVENTS"

static int32_t	process_door_interaction_events(t_map *map, t_camera *cam)
{
	t_process_door_data data;

	data.new_x = cam->pos_x + cosf(cam->dir_angle);
	data.new_y = cam->pos_y + sinf(cam->dir_angle);
	data.index = data.new_x + data.new_y * map->cols;
	if (data.index >= map->cols * map->rows)
	{
		throw_soft_exc(EXC_BOUNDS, ERR_MAP);
		return (-1);
	}
	data.tile_value = map->array[data.index];
	data.tile_is_door = data.tile_value == 2 || data.tile_value == 3;
	data.player_not_in_doorway =
		floorf(cam->pos_x) != data.new_x || floorf(cam->pos_y) != data.new_y;
	if (data.tile_is_door && data.player_not_in_doorway)
	{
		map->array[data.index] = (data.tile_value == 2) ? 3 : 2;
	}
	return (0);
}

static int32_t	process_movements(t_map *map, t_camera *cam,
									const uint8_t *keys_state)
{
	if (keys_state[SDL_SCANCODE_W]
		&& !collides_with_walls(cam, map, FORWARDS))
	{
		cam->pos_x += cosf(cam->dir_angle) / 16;
		cam->pos_y += sinf(cam->dir_angle) / 16;
	}
	if (keys_state[SDL_SCANCODE_S]
		&& !collides_with_walls(cam, map, BACKWARDS))
	{
		cam->pos_x -= cosf(cam->dir_angle) / 16;
		cam->pos_y -= sinf(cam->dir_angle) / 16;
	}
	if (keys_state[SDL_SCANCODE_A]
		&& !collides_with_walls(cam, map, LEFT))
	{
		cam->pos_x += cosf(cam->dir_angle - M_PI / 2) / 16;
		cam->pos_y += sinf(cam->dir_angle - M_PI / 2) / 16;
	}
	if (keys_state[SDL_SCANCODE_D]
		&& !collides_with_walls(cam, map, RIGHT))
	{
		cam->pos_x += cosf(cam->dir_angle + M_PI / 2) / 16;
		cam->pos_y += sinf(cam->dir_angle + M_PI / 2) / 16;
	}
	return (0);
}

static void		process_rotations(t_camera *cam, const uint8_t *keys_state)
{
	if (keys_state[SDL_SCANCODE_LEFT])
	{
		cam->dir_angle -= 0.05;
		if (cam->dir_angle < 0)
			cam->dir_angle += (2 * M_PI);
	}
	if (keys_state[SDL_SCANCODE_RIGHT])
	{
		cam->dir_angle += 0.05;
		if (cam->dir_angle > 2 * M_PI)
			cam->dir_angle -= (2 * M_PI);
	}
}

int32_t			process_keys_state(t_map *map, t_camera *cam,
									const uint8_t *keys_state)
{
	static time_t	timer = 0;

	if (timer == 0)
		time(&timer);
	if (keys_state[SDL_SCANCODE_E])
	{
		if (difftime(time(NULL), timer) >= 1.0)
		{
			time(&timer);
			if (process_door_interaction_events(map, cam) == -1)
			{
				return (-1);
			}
		}
	}
	process_rotations(cam, keys_state);
	if (process_movements(map, cam, keys_state) == -1)
	{
		return (-1);
	}
	return (0);
}
