/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_events.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 22:12:27 by hfrankly          #+#    #+#             */
/*   Updated: 2019/12/14 17:04:20 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	process_termination_events(SDL_Event *e)
{
	if ((e->type == SDL_QUIT) ||
		(e->type == SDL_KEYDOWN && e->key.keysym.sym == SDLK_ESCAPE))
	{
		terminate_successfully("Termination event recieved. "
			"Destroying context and exiting the program\n");
	}
}

static void	process_door_interaction_events(t_context *c, SDL_Event *e)
{
	uint32_t	new_x;
	uint32_t	new_y;
	uint32_t	tile_value;
	bool		player_not_in_doorway;
	bool		tile_is_door;

	if (e->type == SDL_KEYDOWN && e->key.keysym.sym == SDLK_e)
	{
		new_x = c->cam->pos_x + cosf(c->cam->dir_angle);
		new_y = c->cam->pos_y + sinf(c->cam->dir_angle);
		tile_value = c->map->array[new_y * c->map->cols + new_x];
		tile_is_door = tile_value == 2 || tile_value == 3;
		player_not_in_doorway =
			floorf(c->cam->pos_x) != new_x || floorf(c->cam->pos_y) != new_y;
		if (tile_is_door && player_not_in_doorway)
		{
			c->map->array[new_x + new_y * c->map->cols] =
				(tile_value == 2) ? 3 : 2;
		}
	}
}

static void	process_movements(t_context *c, const uint8_t *keys_state)
{
	if (keys_state[SDL_SCANCODE_W] && !collides(c->cam, c->map, FORWARDS))
	{
		c->cam->pos_x += cosf(c->cam->dir_angle) / 10;
		c->cam->pos_y += sinf(c->cam->dir_angle) / 10;
	}
	if (keys_state[SDL_SCANCODE_S] && !collides(c->cam, c->map, BACKWARDS))
	{
		c->cam->pos_x -= cosf(c->cam->dir_angle) / 10;
		c->cam->pos_y -= sinf(c->cam->dir_angle) / 10;
	}
	if (keys_state[SDL_SCANCODE_A] && !collides(c->cam, c->map, RIGHT))
	{
		c->cam->pos_x += cosf(c->cam->dir_angle - M_PI_2) / 20;
		c->cam->pos_y += sinf(c->cam->dir_angle - M_PI_2) / 20;
	}
	if (keys_state[SDL_SCANCODE_D] && !collides(c->cam, c->map, LEFT))
	{
		c->cam->pos_x += cosf(c->cam->dir_angle + M_PI_2) / 20;
		c->cam->pos_y += sinf(c->cam->dir_angle + M_PI_2) / 20;
	}
}

static void	process_rotations(t_context *c, const uint8_t *keys_state)
{
	if (keys_state[SDL_SCANCODE_LEFT])
	{
		c->cam->dir_angle -= 0.05;
		if (c->cam->dir_angle < 0)
			c->cam->dir_angle += (2 * M_PI);
	}
	if (keys_state[SDL_SCANCODE_RIGHT])
	{
		c->cam->dir_angle += 0.05;
		if (c->cam->dir_angle > 2 * M_PI)
			c->cam->dir_angle -= (2 * M_PI);
	}
}

void		process_events(t_context *c, SDL_Event *e)
{
	const uint8_t	*keys_state = SDL_GetKeyboardState(NULL);

	if (e != NULL)
	{
		process_termination_events(e);
		process_door_interaction_events(c, e);
	}
	process_rotations(c, keys_state);
	process_movements(c, keys_state);
}
