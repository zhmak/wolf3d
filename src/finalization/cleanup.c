/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cleanup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 22:01:54 by user              #+#    #+#             */
/*   Updated: 2019/12/14 19:06:01 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	cleanup_textures(t_texture **textures)
{
	t_texture **buff;

	buff = textures;
	while (*textures != NULL)
	{
		free((void *)(*textures)->pixels);
		free(*textures);
		textures++;
	}
	free(buff);
}

void		cleanup_context(t_context *context)
{
	if (context != NULL)
	{
		if (context->win != NULL)
		{
			SDL_DestroyWindow(context->win);
		}
		if (context->map != NULL)
		{
			if (context->map->array != NULL)
			{
				free(context->map->array);
			}
			free(context->map);
		}
		if (context->cam != NULL)
		{
			free(context->cam);
		}
		if (context->textures != NULL)
		{
			cleanup_textures(context->textures);
		}
		free(context);
		SDL_Quit();
	}
}
