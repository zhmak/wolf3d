/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termination.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 17:18:03 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:10:59 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	terminate_with_error(const char *error)
{
	ft_putstr_fd(error, 2);
	exit(EXIT_FAILURE);
}
