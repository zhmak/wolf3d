/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 18:15:54 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 16:40:58 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static const char *g_mem_err = "MALLOC FAILED WHILE SETTING CAMERA PARAMETERS";

t_camera	*init_camera(const t_config *cfg)
{
	t_camera *cam;

	cam = assert_malloc_zero(sizeof(t_camera), g_mem_err);
	cam->dir_angle = (2 * M_PI / 360) * ft_atoi(cfg->dir_angle);
	cam->pos_x = ft_atoi(cfg->pos_x) + 0.5;
	cam->pos_y = ft_atoi(cfg->pos_y) + 0.5;
	return (cam);
}
