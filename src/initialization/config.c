/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 21:03:09 by user              #+#    #+#             */
/*   Updated: 2019/12/14 17:09:43 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define EXC_CFG "[MISSING REQUIRED CONFIG VALUE]\n"
#define ERR_MEM "MALLOC FAILED WHILE READING CONFIG FILE\n"
#define ERR_OPEN "FAILED TO OPEN CONFIG FILE\n"
#define ERR_MEM_TRIM "STRTRIM | WHILE READING CONFIG"
#define ERR_MEM_SPLIT "STRSPLIT | WHILE READING CONFIG"
#define ERR_MEM_DUP "STRDUP | WHILE READING CONFIG"

void			check_config_required_fields(t_config *cfg)
{
	if (cfg->map_path == NULL)
	{
		throw_exc(EXC_CFG, "MAP PATH: map_path");
	}
	if (cfg->dir_angle == NULL)
	{
		throw_exc(EXC_CFG, "DIRECTION ANGLE: dir_angle");
	}
	if (cfg->pos_x == NULL)
	{
		throw_exc(EXC_CFG, "X AXIS POSITION: pos_x");
	}
	if (cfg->pos_y == NULL)
	{
		throw_exc(EXC_CFG, "Y AXIS POSITION: pos_y");
	}
}

static void		check_if_param_and_add_to_cfg(
	t_config *cfg, char *param, char *value)
{
	if (ft_strcmp(param, "map_path") == 0 && cfg->map_path == NULL)
		cfg->map_path = assert_not_null(ft_strdup(value), ERR_MEM_DUP);
	else if (ft_strcmp(param, "dir_angle") == 0 && cfg->dir_angle == NULL)
		cfg->dir_angle = assert_not_null(ft_strdup(value), ERR_MEM_DUP);
	else if (ft_strcmp(param, "pos_x") == 0 && cfg->pos_x == NULL)
		cfg->pos_x = assert_not_null(ft_strdup(value), ERR_MEM_DUP);
	else if (ft_strcmp(param, "pos_y") == 0 && cfg->pos_y == NULL)
		cfg->pos_y = assert_not_null(ft_strdup(value), ERR_MEM_DUP);
}

static void		process_config_line(t_config *cfg, char *str)
{
	char	*param;
	char	*value;
	char	**words;

	words = assert_not_null(ft_strsplit(str, '='), ERR_MEM_SPLIT);
	if (count_words(words) == 2)
	{
		param = assert_not_null(ft_strtrim(words[0]), ERR_MEM_TRIM);
		value = assert_not_null(ft_strtrim(words[1]), ERR_MEM_TRIM);
		check_if_param_and_add_to_cfg(cfg, param, value);
		free(value);
		free(param);
	}
	free_words(words);
}

t_config		*read_config_file(void)
{
	t_config	*cfg;
	int			fd;
	char		*line;

	cfg = NULL;
	cfg = assert_malloc_zero(sizeof(t_config), ERR_MEM);
	fd = try_open_read("config.cfg", ERR_OPEN);
	while (get_next_line(fd, &line))
	{
		process_config_line(cfg, line);
		free(line);
	}
	free(line);
	return (cfg);
}
