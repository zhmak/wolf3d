/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   context.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 22:34:44 by user              #+#    #+#             */
/*   Updated: 2019/12/14 16:41:26 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define EXC_SDL "[SDL EXECUTION EXCEPTION]\n"

#define ERR_MEM "MALLOC FAILED WHILE INITIALIZING CONTEXT\n"
#define ERR_SDL_INIT "FAILED TO INITIALIZE SDL LIBRARY\n"
#define ERR_SDL_WIN "FAILED TO GET SDL WINDOW\n"
#define ERR_SDL_SURF "FAILED TO GET SDL SURFACE PIXELS ARRAY\n"

static void	init_context_sdl(t_context *context)
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		throw_exc(EXC_SDL, ERR_SDL_INIT);
	}
	context->win =
		assert_not_null(
			SDL_CreateWindow(
				"Wolf3D",
				SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				SCREEN_WIDTH, SCREEN_HEIGHT,
				SDL_WINDOW_SHOWN),
			ERR_SDL_WIN);
	context->image =
		assert_not_null(
			SDL_GetWindowSurface(context->win)->pixels,
			ERR_SDL_SURF);
}

t_context	*init_context(void)
{
	t_context *context;

	context = assert_malloc_zero(sizeof(t_context), ERR_MEM);
	context->running = true;
	context->ceiling = (t_rectangle) {
		.left_x = 0,
		.top_y = 0,
		.width = SCREEN_WIDTH,
		.height = SCREEN_HEIGHT / 2,
		.color_argb = COLOR_SKY
	};
	context->floor = (t_rectangle) {
		.left_x = 0,
		.top_y = SCREEN_HEIGHT / 2,
		.width = SCREEN_WIDTH,
		.height = SCREEN_HEIGHT / 2,
		.color_argb = COLOR_GROUND
	};
	init_context_sdl(context);
	return (context);
}
