/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_parsing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 18:07:20 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 18:44:45 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define MEM_ERR "MALLOC FAILED WHILE READING  MAP FILE"

void	set_map_params(const char *map_path, t_map *map)
{
	int32_t		fd;
	char		*buffer;

	if (map_path == NULL)
		terminate_with_error(
		"Error: map path is not specified in config file\n");
	if ((fd = open(map_path, O_RDONLY)) < 0)
		terminate_with_error(
		"Error: there is no map file as specified in config\n");
	if (get_next_line(fd, &buffer) == -1)
		terminate_with_error("Error: map file is incorrect\n");
	map->cols = ft_strlen(buffer);
	free(buffer);
	map->rows = 1;
	while (get_next_line(fd, &buffer))
	{
		map->rows++;
		if (ft_strlen(buffer) != map->cols)
		{
			terminate_with_error(
		"Wrong map structure\nIt must be like a rectangle\n");
		}
		free(buffer);
	}
	free(buffer);
}

void	set_map_array(const char *map_path, t_map *map)
{
	int32_t		fd;
	char		*buffer;
	char		*buffer_free;
	uint32_t	*array;

	buffer_free = NULL;
	array = map->array;
	fd = open(map_path, O_RDONLY);
	while (get_next_line(fd, &buffer))
	{
		buffer_free = buffer;
		while (*buffer)
		{
			*array = *buffer - '0';
			array++;
			buffer++;
		}
		free(buffer_free);
	}
	free(buffer);
}

t_map	*read_map_file(const char *map_path)
{
	t_map *map;

	map = assert_malloc_zero(sizeof(t_map), MEM_ERR);
	set_map_params(map_path, map);
	map->array = assert_malloc_zero(
		sizeof(uint32_t) * map->rows * map->cols,
		MEM_ERR);
	set_map_array(map_path, map);
	return (map);
}
