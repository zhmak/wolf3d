/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_validation.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/26 15:45:15 by hfrankly          #+#    #+#             */
/*   Updated: 2019/12/14 16:53:16 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define ERR_DOORS "DOORS MUST BE BORDERED BY 2 OPPOSITE WALLS\n"
#define ERR_BORDER "MAP MUST BE BORDERED BY WALLS (1s)\n"
#define ERR_PLAYER "PLAYER MUST BE PLACED INSIDE THE MAP AND IN OPEN SPACE"

/*
** returns false if player position is outside of the map or inside of a wall
** otherwise returns true
*/

static bool	has_proper_player_position(const t_map *map,
	uint32_t player_tile_x, uint32_t player_tile_y)
{
	if (player_tile_x > map->cols - 1 ||
		player_tile_y > map->rows - 1)
		return (false);
	if (map->array[player_tile_x + player_tile_y * map->cols] != 0)
		return (false);
	return (true);
}

static bool	has_walled_doors(const t_map *map)
{
	uint32_t	row;
	uint32_t	col;

	row = 1;
	while (row < map->rows - 1)
	{
		col = 1;
		while (col < map->cols - 1)
		{
			if (map->array[row * map->cols + col] == 2 ||
				map->array[row * map->cols + col] == 3)
			{
				if ((map->array[(row - 1) * map->cols + col] != 1 ||
					map->array[(row + 1) * map->cols + col] != 1)
					&&
					(map->array[row * map->cols + col - 1] != 1 ||
					map->array[row * map->cols + col + 1] != 1))
					return (false);
			}
			col++;
		}
		row++;
	}
	return (true);
}

/*
** returns true if @map is bordered by 1s
** otherwise returns false
*/

static bool	has_walled_borders(const t_map *map)
{
	uint32_t	*arr;
	uint32_t	cols;
	uint32_t	rows;
	uint32_t	tile;

	arr = map->array;
	cols = map->cols;
	rows = map->rows;
	tile = 0;
	while (tile < cols)
	{
		if (arr[tile] != 1 ||
			arr[tile + cols * (rows - 1)] != 1)
			return (false);
		tile++;
	}
	tile = 0;
	while (tile < rows)
	{
		if (arr[tile * cols] != 1 ||
			arr[tile * cols + (cols - 1)] != 1)
			return (false);
		tile++;
	}
	return (true);
}

void		validate_map_and_camera(const t_map *map, const t_camera *cam)
{
	assert_true(has_walled_borders(map), ERR_BORDER);
	assert_true(has_walled_doors(map), ERR_DOORS);
	assert_true(
		has_proper_player_position(
			map,
			(uint32_t)cam->pos_x,
			(uint32_t)cam->pos_y),
		ERR_PLAYER);
}
