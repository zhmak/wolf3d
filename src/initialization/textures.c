/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 13:07:44 by hfrankly          #+#    #+#             */
/*   Updated: 2020/01/22 15:02:41 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define ERR_MEM "MALLOC FAILED WHILE READING TEXTURES"
#define ERR_OPEN "FILE OPENING FAILED WHILE READING TEXTURES"
#define ERR_FRMT "WRONG FILE FORMAT WHILE READING TEXTURES"
#define ERR_PXL "WRONG PIXELS COUNT WHILE READING TEXTURES"
#define ERR_SPLT "STRSPLIT | WHILE READING TEXTURES"
#define ERR_VALUES "INCORRECT NUMBER OF VALUES IN ONE LINE IN READING TEXTURES"

/*
** parses @line, adds values to @pixels from the start, returns num of pixels
*/

static uint32_t		add_pixels_to_array(char *line, uint32_t *pixels,
								uint32_t max_size, uint32_t current_pixels)
{
	uint32_t	pixels_added;
	char		**values;
	uint32_t	i;

	values = assert_not_null(ft_strsplit(line, ' '), ERR_SPLT);
	assert_true(count_words(values) % 3 == 0, ERR_VALUES);
	assert_true(count_words(values) / 3 + current_pixels <= max_size,
				ERR_VALUES);
	i = 0;
	pixels_added = 0;
	while (values[i])
	{
		*pixels += ft_atoi(values[i]) << 16;
		i++;
		*pixels += ft_atoi(values[i]) << 8;
		i++;
		*pixels += ft_atoi(values[i]);
		i++;
		pixels++;
		pixels_added++;
	}
	free_words(values);
	return (pixels_added);
}

/*
** allocates @width * @height array of pixels
** reads file from provided @fd and writes its values into array of pixels
** returns array of pixels
*/

static uint32_t		*get_pixels_array(int fd, uint32_t width, uint32_t height)
{
	uint32_t	*pixels;
	char		*line;
	uint32_t	num_of_pixels;
	uint32_t	size;

	pixels = assert_malloc_zero(sizeof(uint32_t) * width * height, ERR_MEM);
	num_of_pixels = 0;
	size = width * height;
	while (get_next_line(fd, &line))
	{
		num_of_pixels += add_pixels_to_array(line, pixels + num_of_pixels,
											size, num_of_pixels);
		free(line);
	}
	assert_true(num_of_pixels == width * height, ERR_PXL);
	free(line);
	return (pixels);
}

static t_texture	*get_texture_from_file(char *texture_path)
{
	t_texture	*texture;
	int			fd;
	char		*line;

	texture = assert_malloc_zero(sizeof(t_texture), ERR_MEM);
	fd = try_open_read(texture_path, ERR_OPEN);
	get_next_line(fd, &line);
	assert_true(ft_strequ(line, "P3"), ERR_FRMT);
	free(line);
	get_next_line(fd, &line);
	texture->width = ft_atoi(line);
	texture->height = ft_atoi(line + 4);
	free(line);
	texture->pixels = get_pixels_array(fd, texture->width, texture->height);
	return (texture);
}

t_texture			**cache_textures_from_files(void)
{
	t_texture **textures;

	textures = assert_malloc_zero(sizeof(t_texture *) * 6, ERR_MEM);
	textures[0] = get_texture_from_file("textures/brick.ppm");
	textures[1] = get_texture_from_file("textures/cobb.ppm");
	textures[2] = get_texture_from_file("textures/door.ppm");
	textures[3] = get_texture_from_file("textures/moss.ppm");
	textures[4] = get_texture_from_file("textures/plank.ppm");
	textures[5] = NULL;
	return (textures);
}
