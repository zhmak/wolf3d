/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 12:44:14 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 18:47:42 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static int32_t	start_multithreaded_execution(t_context *с)
{
	const uint8_t	*keys_state;
	SDL_Event		event;
	pthread_t		rendering_thread;
	void			*rendering_error;

	keys_state = SDL_GetKeyboardState(NULL);
	pthread_create(&rendering_thread, NULL, (void *)render_multithreaded, с);
	while (с->running)
	{
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT
			|| process_keys_state(с->map, с->cam, keys_state) == -1
			|| (event.type == SDL_KEYDOWN
			&& event.key.keysym.sym == SDLK_ESCAPE))
		{
			с->running = false;
			break ;
		}
		SDL_Delay(20);
	}
	rendering_error = NULL;
	pthread_join(rendering_thread, &rendering_error);
	if (rendering_error != NULL)
		return (-1);
	return (0);
}

static void		cleanup_config(t_config *cfg)
{
	free(cfg->dir_angle);
	free(cfg->map_path);
	free(cfg->pos_x);
	free(cfg->pos_y);
	free(cfg);
}

int				main(void)
{
	int32_t		result;
	t_config	*cfg;
	t_context	*context;

	cfg = read_config_file();
	check_config_required_fields(cfg);
	context = init_context();
	context->cam = init_camera(cfg);
	context->map = read_map_file(cfg->map_path);
	validate_map_and_camera(context->map, context->cam);
	context->textures = cache_textures_from_files();
	cleanup_config(cfg);
	result = start_multithreaded_execution(context);
	cleanup_context(context);
	if (result == -1)
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
