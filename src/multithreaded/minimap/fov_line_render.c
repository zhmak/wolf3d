/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_line.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 18:55:12 by hfrankly          #+#    #+#             */
/*   Updated: 2019/11/03 20:42:46 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		render_line_high_angle(
	t_context *context, float k, int signx, int signy)
{
	uint_fast32_t	x;
	uint_fast32_t	y;
	uint_fast16_t	wall_size;
	float			error;

	wall_size = (context->map->cols > context->map->rows)
		? MM_SIZE / context->map->cols
		: MM_SIZE / context->map->rows;
	x = context->cam->pos_x * wall_size;
	y = context->cam->pos_y * wall_size;
	error = k;
	while (x < MM_SIZE || y < MM_SIZE)
	{
		error += k;
		if (error >= 1)
		{
			error--;
			x += signx;
		}
		y += signy;
		if (context->image[y * SCREEN_WIDTH + x] == MM_COLOR_WALL)
			break ;
		context->image[y * SCREEN_WIDTH + x] = MM_COLOR_FOV;
	}
}

static void		render_line_low_angle(
	t_context *context, float k, int signx, int signy)
{
	uint_fast32_t	x;
	uint_fast32_t	y;
	uint_fast16_t	wall_size;
	float			error;

	wall_size = (context->map->cols > context->map->rows)
		? MM_SIZE / context->map->cols
		: MM_SIZE / context->map->rows;
	x = context->cam->pos_x * wall_size;
	y = context->cam->pos_y * wall_size;
	error = k;
	while (x < MM_SIZE || y < MM_SIZE)
	{
		error += k;
		if (error >= 1)
		{
			error--;
			y += signy;
		}
		x += signx;
		if (context->image[y * SCREEN_WIDTH + x] == MM_COLOR_WALL)
			break ;
		context->image[y * SCREEN_WIDTH + x] = MM_COLOR_FOV;
	}
}

void			render_line(t_context *context, float angle)
{
	if (angle <= M_PI_2 / 2 && angle >= 0)
		render_line_low_angle(context, fabs(tanf(angle)), 1, 1);
	else if (angle <= M_PI_2 && angle >= M_PI_2 / 2)
		render_line_high_angle(context, fabs(1 / tanf(angle)), 1, 1);
	else if (angle <= M_PI - M_PI_2 / 2 && angle >= M_PI_2)
		render_line_high_angle(context, fabs(1 / tanf(angle)), -1, 1);
	else if (angle <= M_PI && angle >= M_PI - M_PI_2 / 2)
		render_line_low_angle(context, fabs(tanf(angle)), -1, 1);
	else if (angle <= M_PI + M_PI_2 / 2 && angle >= M_PI)
		render_line_low_angle(context, fabs(tanf(angle)), -1, -1);
	else if (angle <= M_PI + M_PI_2 && angle >= M_PI + M_PI_2 / 2)
		render_line_high_angle(context, fabs(1 / tanf(angle)), -1, -1);
	else if (angle <= 2 * M_PI - M_PI_2 / 2 && angle >= M_PI + M_PI_2)
		render_line_high_angle(context, fabs(1 / tanf(angle)), 1, -1);
	else if (angle <= 2 * M_PI && angle >= 2 * M_PI - M_PI_2 / 2)
		render_line_low_angle(context, fabs(tanf(angle)), 1, -1);
}
