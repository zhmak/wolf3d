/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_render.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 11:34:50 by hfrankly          #+#    #+#             */
/*   Updated: 2019/12/14 17:06:19 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		render_minimap_background(
	t_context *context, uint_fast16_t wall_size)
{
	t_rectangle		minimap;

	minimap = (t_rectangle){0, 0, wall_size * context->map->cols,
	wall_size * context->map->rows, MM_COLOR_BACKGROUND};
	render_rectangle_surface(context, &minimap);
}

static void		render_minimap_walls(
	t_context *context, uint_fast16_t wall_size)
{
	uint_fast16_t	i;
	uint_fast16_t	j;
	uint_fast32_t	offset;

	i = -1;
	while (++i < context->map->rows)
	{
		j = 0;
		offset = i * context->map->cols;
		while (j < context->map->cols)
		{
			if (context->map->array[offset + j] == 1 ||
				context->map->array[offset + j] == 2)
				render_rectangle_surface(context, &(t_rectangle){j * wall_size,
				i * wall_size, wall_size, wall_size, MM_COLOR_WALL});
			else if (context->map->array[offset + j] == 3)
				render_rectangle_surface(context, &(t_rectangle){j * wall_size,
				i * wall_size, wall_size, wall_size, MM_COLOR_DOOR});
			j++;
		}
	}
}

static void		render_player_fov(t_context *context)
{
	float			cur_angle;
	float			end_angle;

	cur_angle = context->cam->dir_angle - M_PI / 4;
	end_angle = context->cam->dir_angle + M_PI / 4;
	if (end_angle < 0)
		end_angle += 2 * M_PI;
	while (cur_angle < end_angle)
	{
		cur_angle += M_PI / 180 / 8;
		if (cur_angle < 0)
			render_line(context, cur_angle + 2 * M_PI);
		if (cur_angle >= 2 * M_PI)
			render_line(context, cur_angle - 2 * M_PI);
		else
			render_line(context, cur_angle);
	}
}

static void		render_minimap_player(
	t_context *context, uint_fast16_t wall_size)
{
	uint_fast16_t	player_position_x;
	uint_fast16_t	player_position_y;

	player_position_x = context->cam->pos_x * wall_size - 3;
	player_position_y = context->cam->pos_y * wall_size - 3;
	render_rectangle_surface(context, &(t_rectangle){player_position_x,
	player_position_y, 6, 6, MM_COLOR_PLAYER});
	render_player_fov(context);
}

void			render_minimap(t_context *context)
{
	uint_fast16_t	wall_size;

	wall_size = (context->map->cols > context->map->rows)
	? MM_SIZE / context->map->cols
	: MM_SIZE / context->map->rows;
	render_minimap_background(context, wall_size);
	render_minimap_walls(context, wall_size);
	render_minimap_player(context, wall_size);
}
