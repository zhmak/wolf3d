/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hfrankly <hfrankly@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 18:10:01 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 17:01:03 by hfrankly         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define ERR_WINDOW	"FAILED TO UPDATE SDL WINDOW SURFACE\n"

static void	calculate_framerate(void)
{
	static uint32_t	frames = 0;
	static time_t	timer = 0;

	if (timer == 0)
		time(&timer);
	frames++;
	if (difftime(time(NULL), timer) >= 1.0)
	{
		ft_putstr("FPS:");
		ft_putnbr(frames);
		ft_putchar('\n');
		time(&timer);
		frames = 0;
	}
}

static void	update_window(SDL_Window *window)
{
	assert_true(SDL_UpdateWindowSurface(window) == 0, ERR_WINDOW);
}

static int	cast_rays_multithreaded(t_multithreading_data *d)
{
	uint32_t i;

	i = 0;
	while (i < NUM_THREADS)
	{
		pthread_create(&d->threads[i].pthread,
			NULL, (void *)cast_rays, &d->threads[i]);
		i++;
	}
	i = 0;
	while (i < NUM_THREADS)
	{
		pthread_join(d->threads[i].pthread, &d->threads[i].error);
		i++;
	}
	i = 0;
	while (i < NUM_THREADS)
	{
		if (d->threads[i].error != NULL)
			return (-1);
		i++;
	}
	return (0);
}

static void	init_multithreading_data(t_multithreading_data *d, t_context *c)
{
	uint32_t i;

	ft_memcpy(&d->rendering_context, c, sizeof(t_context));
	ft_memcpy(&d->rendering_camera, c->cam, sizeof(t_camera));
	d->rendering_context.cam = &d->rendering_camera;
	i = 0;
	while (i < NUM_THREADS)
	{
		d->threads[i].context = &d->rendering_context;
		d->threads[i].num = i;
		d->threads[i].error = NULL;
		i++;
	}
}

void		*render_multithreaded(t_context *context)
{
	t_multithreading_data	data;

	init_multithreading_data(&data, context);
	while (context->running)
	{
		render_rectangle_surface(context, &context->ceiling);
		render_rectangle_surface(context, &context->floor);
		if (cast_rays_multithreaded(&data) == -1)
		{
			context->running = false;
			break ;
		}
		render_minimap(context);
		update_window(context->win);
		data.rendering_camera.dir_angle = context->cam->dir_angle;
		data.rendering_camera.pos_x = context->cam->pos_x;
		data.rendering_camera.pos_y = context->cam->pos_y;
		calculate_framerate();
	}
	return (NULL);
}
