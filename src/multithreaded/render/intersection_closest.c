/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersection_closest.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/01 19:59:56 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 16:48:38 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define EXC_BOUNDS "[ARRAY OUT OF BOUNDS]"
#define ERR_MAP "MAP MEM INDEX OUT OF BOUNDS ON INTERSECTION SEARCH"

static int				set_array_index(
	t_map *map, t_first_intersection_data *d)
{
	d->index = d->current_tile_x + d->current_tile_y * map->cols;
	if (d->index >= d->map_mem_size)
	{
		return (throw_soft_exc(EXC_BOUNDS, ERR_MAP));
	}
	return (0);
}

static t_intersection	*first_wall_intersection(t_map *map,
	t_intersection *ver, t_intersection *hor, t_first_intersection_data *d)
{
	while (true)
	{
		if (ver->abs_dist_x <= hor->abs_dist_x)
		{
			d->current_tile_x += ver->step_x;
			if (set_array_index(map, d) == -1)
				return (NULL);
			if (map->array[d->index] == 1 || map->array[d->index] == 2)
				return (ver);
			ver->current_pos_x += ver->step_x;
			ver->current_pos_y += ver->step_y;
			ver->abs_dist_x += d->abs_ver_step_x;
		}
		else
		{
			d->current_tile_y += hor->step_y;
			if (set_array_index(map, d) == -1)
				return (NULL);
			if (map->array[d->index] == 1 || map->array[d->index] == 2)
				return (hor);
			hor->current_pos_x += hor->step_x;
			hor->current_pos_y += hor->step_y;
			hor->abs_dist_x += fabsf(hor->step_x);
		}
	}
}

t_intersection			*closest_wall_intersection(
	t_intersection *ver, t_intersection *hor, t_map *map, t_ray *ray)
{
	t_intersection				*closest;
	t_first_intersection_data	d;

	d.current_tile_x = (uint32_t)ray->init_x;
	d.current_tile_y = (uint32_t)ray->init_y;
	d.abs_hor_step_x = fabsf(hor->step_x);
	d.abs_ver_step_x = fabsf(ver->step_x);
	d.map_mem_size = map->cols * map->rows;
	closest = first_wall_intersection(map, ver, hor, &d);
	closest->current_tile_x = d.current_tile_x;
	closest->current_tile_y = d.current_tile_y;
	return (closest);
}
