/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersections.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 14:40:07 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 16:40:47 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		first_horizontal_tile_intersection(t_intersection *i, t_ray *ray)
{
	float tan;
	float first_step_y;
	float first_step_x;

	tan = tanf(ray->dir_angle);
	i->step_y = ray->y_dir;
	i->step_x = ray->x_dir / tan;
	if (ray->y_dir == 1)
	{
		i->current_pos_y = ceilf(ray->init_y);
		first_step_y = i->current_pos_y - ray->init_y;
	}
	else
	{
		i->current_pos_y = floorf(ray->init_y);
		first_step_y = ray->init_y - i->current_pos_y;
	}
	first_step_x = first_step_y / tan;
	i->abs_dist_x = first_step_x;
	if (ray->x_dir == 1)
		i->current_pos_x = ray->init_x + i->abs_dist_x;
	else
		i->current_pos_x = ray->init_x - i->abs_dist_x;
}

void		first_vertical_tile_intersection(t_intersection *i, t_ray *ray)
{
	float tan;
	float first_step_y;
	float first_step_x;

	tan = tanf(ray->dir_angle);
	i->step_y = ray->y_dir * tan;
	i->step_x = ray->x_dir;
	if (ray->x_dir == 1)
	{
		i->current_pos_x = ceilf(ray->init_x);
		first_step_x = i->current_pos_x - ray->init_x;
	}
	else
	{
		i->current_pos_x = floorf(ray->init_x);
		first_step_x = ray->init_x - i->current_pos_x;
	}
	first_step_y = first_step_x * tan;
	i->abs_dist_x = first_step_x;
	if (ray->y_dir == 1)
		i->current_pos_y = ray->init_y + first_step_y;
	else
		i->current_pos_y = ray->init_y - first_step_y;
}
