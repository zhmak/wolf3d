/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pixel_mapping.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 19:17:17 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 16:59:29 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

#define EXC_BOUNDS	"[ARRAY OUT OF BOUNDS]\n"
#define ERR_RECT	"IMAGE MEM INDEX OUT OF BOUNDS WHILE RENDERING RECTANGLE\n"
#define ERR_IMAGE	"IMAGE MEM INDEX OUT OF BOUNDS WHILE MAPPING TEXTURES\n"
#define ERR_TEXTURE	"TEXTURE MEM INDEX OUT OF BOUNDS WHILE MAPPING TEXTURES\n"

void		render_rectangle_surface(t_context *context, t_rectangle *rect)
{
	t_rectangle_render_data	data;
	uint32_t				row_offset;
	uint32_t				image_index;
	uint32_t				image_mem_size;

	image_mem_size = SCREEN_HEIGHT * SCREEN_WIDTH;
	data.max_x = rect->left_x + rect->width;
	data.y = rect->top_y;
	data.max_y = rect->top_y + rect->height;
	while (data.y < data.max_y)
	{
		data.x = rect->left_x;
		row_offset = data.y * SCREEN_WIDTH;
		while (data.x < data.max_x)
		{
			image_index = data.x + row_offset;
			if (image_index >= image_mem_size)
				throw_exc(EXC_BOUNDS, ERR_RECT);
			context->image[image_index] = rect->color_argb;
			data.x++;
		}
		data.y++;
	}
}

static void	map_texture_to_image(uint32_t *img, t_texture *text,
		t_texture_mapping_data *data)
{
	uint32_t image_index;
	uint32_t texture_index;
	uint32_t image_mem_size;
	uint32_t text_mem_size;

	image_mem_size = SCREEN_HEIGHT * SCREEN_WIDTH;
	text_mem_size = text->height * text->width;
	while (data->image_y < data->image_max_y && data->text_y < data->text_max_y)
	{
		image_index = data->image_x + data->image_y * SCREEN_WIDTH;
		texture_index = data->text_x + (uint32_t)data->text_y * text->width;
		if (image_index >= image_mem_size)
			throw_exc(EXC_BOUNDS, ERR_IMAGE);
		if (texture_index >= text_mem_size)
			throw_exc(EXC_BOUNDS, ERR_TEXTURE);
		img[image_index] = text->pixels[texture_index];
		data->text_y += data->text_step_y;
		data->image_y++;
	}
}

void		render_textured_line(uint32_t *image, t_ver_line *line,
					float intersection_coordinate)
{
	t_texture_mapping_data data;

	data.image_x = line->x;
	data.image_y = line->top_y;
	data.text_x = line->text->width * ft_modf(intersection_coordinate);
	if (line->top_y)
	{
		data.text_y = 0;
		data.text_max_y = line->text->height;
		data.text_step_y = line->text->height / (float)line->height;
		data.image_max_y = line->top_y + line->height;
	}
	else
	{
		data.text_y =
			(line->text->height - line->text->height * line->distance) / 2;
		data.text_max_y = (uint32_t)(line->text->height - data.text_y);
		data.text_step_y = (data.text_max_y - data.text_y) / SCREEN_HEIGHT;
		data.image_max_y = SCREEN_HEIGHT;
	}
	map_texture_to_image(image, line->text, &data);
}
