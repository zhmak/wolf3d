/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_casting.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 16:58:17 by eloren-l          #+#    #+#             */
/*   Updated: 2019/12/14 16:53:27 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static t_ver_line	get_line(
	t_intersection *hor, t_texture **textures,
	t_intersection *closest, t_ray *ray)
{
	t_ver_line		line;

	if (closest == hor)
	{
		line.distance = (fabs(closest->current_pos_y - ray->init_y)
			/ sinf(ray->dir_angle))
			* cos(ray->center_angle - ray->unreduced_angle);
		line.text = (ray->y_dir == 1) ? textures[COBB] : textures[MOSS];
	}
	else
	{
		line.distance = (fabs(closest->current_pos_x - ray->init_x)
			/ cosf(ray->dir_angle))
			* cos(ray->center_angle - ray->unreduced_angle);
		line.text = (ray->x_dir == 1) ? textures[PLANK] : textures[BRICK];
	}
	line.height = SCREEN_HEIGHT / line.distance;
	if (line.height < 10)
		line.height = 10;
	else if (line.height > SCREEN_HEIGHT)
		line.height = SCREEN_HEIGHT;
	line.top_y = SCREEN_HEIGHT / 2 - line.height / 2;
	return (line);
}

static int			set_texture_if_door(
		t_intersection *i, t_map *map, t_texture **textures, t_ver_line *line)
{
	uint32_t index;

	index = i->current_tile_x + i->current_tile_y * map->cols;
	if (index >= map->cols * map->rows)
	{
		return (-1);
	}
	if (map->array[index] == 2)
	{
		line->text = textures[DOOR];
	}
	return (0);
}

static int			draw_line(
	t_context *c, t_ray *ray, uint32_t width_x)
{
	t_intersection	ver;
	t_intersection	hor;
	t_intersection	*closest;
	t_ver_line		line;

	first_horizontal_tile_intersection(&hor, ray);
	first_vertical_tile_intersection(&ver, ray);
	if ((closest = closest_wall_intersection(&ver, &hor, c->map, ray)) == NULL)
		return (-1);
	line = get_line(&hor, c->textures, closest, ray);
	line.x = width_x;
	if (set_texture_if_door(closest, c->map, c->textures, &line) == -1)
		return (-1);
	if (closest == &hor)
		render_textured_line(c->image, &line, closest->current_pos_x);
	else
		render_textured_line(c->image, &line, closest->current_pos_y);
	return (0);
}

t_ray				get_ray(t_camera *cam, int32_t x)
{
	t_ray ray;
	float ray_x;
	float ray_y;
	float atan;
	float y;

	y = SCREEN_WIDTH / 2;
	ray_x = y * cam->dir_cos - x * cam->dir_sin;
	ray_y = y * cam->dir_sin + x * cam->dir_cos;
	atan = atan2f(ray_y, ray_x);
	ray.unreduced_angle = (atan > 0) ? atan : 2 * M_PI + atan;
	ray.dir_angle = ray.unreduced_angle;
	ray.center_angle = cam->dir_angle;
	ray.init_x = cam->pos_x;
	ray.init_y = cam->pos_y;
	return (ray);
}

void				*cast_rays(t_thread *t)
{
	t_ray			ray;
	t_camera		*cam;
	int32_t			x;
	int32_t			max_x;

	cam = t->context->cam;
	cam->dir_cos = cos(cam->dir_angle);
	cam->dir_sin = sin(cam->dir_angle);
	x = (t->num * (SCREEN_WIDTH / NUM_THREADS)) - SCREEN_WIDTH / 2;
	max_x = x + SCREEN_WIDTH / NUM_THREADS;
	while (x < max_x)
	{
		ray = get_ray(cam, x);
		set_ray_directions(&ray);
		if (draw_line(t->context, &ray, x + SCREEN_WIDTH / 2) == -1)
		{
			return ((void *)-1);
		}
		x++;
	}
	return (NULL);
}
