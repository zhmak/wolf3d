/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_ray_directions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 20:25:09 by eloren-l          #+#    #+#             */
/*   Updated: 2019/11/03 20:26:08 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	set_ray_directions_next(t_ray *ray)
{
	if (ray->dir_angle >= M_PI_2 && ray->dir_angle <= M_PI)
	{
		ray->x_dir = -1;
		ray->y_dir = 1;
		ray->dir_angle = M_PI_2 - (ray->dir_angle - M_PI_2);
	}
	else if (ray->dir_angle >= M_PI && ray->dir_angle <= M_PI + M_PI_2)
	{
		ray->x_dir = -1;
		ray->y_dir = -1;
		ray->dir_angle = ray->dir_angle - M_PI;
	}
	else if (ray->dir_angle >= M_PI + M_PI_2 && ray->dir_angle <= 2 * M_PI)
	{
		ray->x_dir = 1;
		ray->y_dir = -1;
		ray->dir_angle = M_PI_2 - (ray->dir_angle - M_PI - M_PI_2);
	}
}

void		set_ray_directions(t_ray *ray)
{
	if (ray->dir_angle < 0)
	{
		ray->x_dir = 1;
		ray->y_dir = -1;
		ray->dir_angle = fabsf(ray->dir_angle);
	}
	else if (ray->dir_angle >= 2 * M_PI)
	{
		ray->x_dir = 1;
		ray->y_dir = 1;
		ray->dir_angle -= 2 * M_PI;
	}
	else if (ray->dir_angle >= 0 && ray->dir_angle <= M_PI_2)
	{
		ray->x_dir = 1;
		ray->y_dir = 1;
	}
	else
	{
		set_ray_directions_next(ray);
	}
}
