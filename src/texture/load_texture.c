/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_texture.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 13:07:44 by hfrankly          #+#    #+#             */
/*   Updated: 2019/12/14 17:19:40 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int			ft_space_pos(char *str)
{
	int			res;

	res = 0;
	while (str[res] && str[res] != ' ')
		res++;
	if (str[res] == ' ')
		res++;
	return (res);
}

void		set_pixels(t_texture *texture, int fd)
{
	char		*str;
	char		*tmpstr;
	uint32_t	*pixels;
	uint32_t	i;

	i = 0;
	pixels = (uint32_t *)texture->pixels;
	while (get_next_line(fd, &str))
	{
		tmpstr = str;
		while (*tmpstr)
		{
			pixels[i] = ft_atoi(tmpstr) * 65536;
			tmpstr += ft_space_pos(tmpstr);
			pixels[i] += ft_atoi(tmpstr) * 256;
			tmpstr += ft_space_pos(tmpstr);
			pixels[i] += ft_atoi(tmpstr);
			tmpstr += ft_space_pos(tmpstr);
			i++;
		}
		free(str);
	}
	if (i != texture->width * texture->height)
		terminate_with_error("Wrong pixel count\n");
}

t_texture	*read_texture(char *texture_path)
{
	int			fd;
	t_texture	*texture;
	char		*str;

	if (!(texture = (t_texture*)malloc(sizeof(t_texture))))
		exit(0);
	if ((fd = open(texture_path, O_RDONLY)) == -1)
		terminate_with_error("Wrong texture path\n");
	get_next_line(fd, &str);
	if (ft_strcmp(str, "P3"))
		terminate_with_error("Wrong texture format\n");
	free(str);
	get_next_line(fd, &str);
	texture->width = ft_atoi(str);
	texture->height = ft_atoi(str + 4);
	free(str);
	if (!(texture->pixels = malloc(texture->width * texture->height *
		sizeof(uint32_t))))
		exit(0);
	set_pixels(texture, fd);
	return (texture);
}

void		init_textures(void)
{
	t_context	*context;

	context = get_context();
	if (!(context->textures = (t_texture**)malloc(sizeof(t_texture*) * 6)))
		exit(0);
	context->textures[0] = read_texture(
		"textures/brick.ppm");
	context->textures[1] = read_texture(
		"textures/cobb.ppm");
	context->textures[2] = read_texture(
		"textures/door.ppm");
	context->textures[3] = read_texture(
		"textures/moss.ppm");
	context->textures[4] = read_texture(
		"textures/plank.ppm");
	context->textures[5] = NULL;
}
